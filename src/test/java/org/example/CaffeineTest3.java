package org.example;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.RemovalListener;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class CaffeineTest3 {


    @DisplayName("测试移除cache,invalidate(key)方法")
    @Test
    public void testRemoveCache() {
        Cache<String, String> cache = Caffeine.newBuilder()
                .expireAfterWrite(50, TimeUnit.SECONDS)
                .maximumSize(100)
                .build();

        cache.put("test1", "value1");
        cache.put("test2", "value2");
        cache.put("test3", "value3");
        cache.put("test4", "value4");

        System.out.println(cache.asMap()); //{test1=value1, test4=value4, test2=value2, test3=value3}
        cache.invalidate("test1"); //移除指定key的Entry
        System.out.println(cache.asMap()); //{test4=value4, test2=value2, test3=value3}
    }


    @DisplayName("测试移除cache,invalidateAll()方法")
    @Test
    public void testRemoveCache2() {
        Cache<String, String> cache = Caffeine.newBuilder()
                .expireAfterWrite(50, TimeUnit.SECONDS)
                .maximumSize(100)
                .build();

        cache.put("test1", "value1");
        cache.put("test2", "value2");
        cache.put("test3", "value3");
        cache.put("test4", "value4");

        System.out.println(cache.asMap()); //{test1=value1, test4=value4, test2=value2, test3=value3}
        cache.invalidateAll(); //移除所有的cache
        System.out.println(cache.asMap()); //{}
    }


    @DisplayName("测试移除cache,invalidateAll(keys)方法")
    @Test
    public void testRemoveCache3() {
        Cache<String, String> cache = Caffeine.newBuilder()
                .expireAfterWrite(50, TimeUnit.SECONDS)
                .maximumSize(100)
                .build();

        cache.put("test1", "value1");
        cache.put("test2", "value2");
        cache.put("test3", "value3");
        cache.put("test4", "value4");

        System.out.println(cache.asMap()); //{test1=value1, test4=value4, test2=value2, test3=value3}
        cache.invalidateAll(Arrays.asList("test1", "test2")); //批量移除指定key的Entry
        System.out.println(cache.asMap()); //{test4=value4, test3=value3}
    }


    @Test
    public void testRemovalListener() {
        Cache<String, String> cache = Caffeine.newBuilder()
                .expireAfterWrite(50, TimeUnit.SECONDS)
                .maximumSize(100)
                .removalListener((RemovalListener<String, String>) (key, value, cause) -> System.out.println(Thread.currentThread().getName() + "--" + MessageFormat.format("key:[{0}],value:[{1}],cause:[{2}]", key, value, cause)))
                .build();

        cache.put("test1", "value1");
        cache.put("test2", "value2");
        cache.put("test3", "value3");
        cache.put("test4", "value4");


        System.out.println(cache.asMap()); //{test1=value1, test4=value4, test2=value2, test3=value3}
        cache.invalidate("test1"); //移除指定key的Entry
        System.out.println(cache.asMap()); //{test4=value4, test2=value2, test3=value3}
        //removalListener打印：ForkJoinPool.commonPool-worker-3--key:[test1],value:[value1],cause:[EXPLICIT]
    }


    @Test
    public void testRemovalListener2() {
        Cache<String, String> cache = Caffeine.newBuilder()
                .expireAfterWrite(50, TimeUnit.SECONDS)
                .maximumSize(1)
                .removalListener((RemovalListener<String, String>) (key, value, cause) -> System.out.println(MessageFormat.format("key:[{0}],value:[{1}],cause:[{2}]", key, value, cause)))
                .build();

        cache.put("test1", "value1");
        cache.put("test2", "value2");

        System.out.println(cache.asMap()); //{test1=value1, test4=value4, test2=value2, test3=value3}
        cache.cleanUp();
        System.out.println(cache.asMap()); //{test4=value4, test2=value2, test3=value3}
        //removalListener打印：key:[test1],value:[value1],cause:[SIZE]
    }


    @Test
    public void testRemovalListener3() throws InterruptedException {
        Cache<String, String> cache = Caffeine.newBuilder()
                .expireAfterWrite(1, TimeUnit.SECONDS)
                .maximumSize(10)
                .removalListener((RemovalListener<String, String>) (key, value, cause) -> System.out.println(MessageFormat.format("key:[{0}],value:[{1}],cause:[{2}]", key, value, cause)))
                .build();

        cache.put("test1", "value1");
        System.out.println(cache.asMap()); //{test1=value1}
        Thread.sleep(1000);
        cache.cleanUp();
        System.out.println(cache.asMap()); //{}
        //removalListener打印：key:[test1],value:[value1],cause:[EXPIRED]
    }


}
