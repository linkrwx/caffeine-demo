package org.example;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.Expiry;
import com.github.benmanes.caffeine.cache.Weigher;
import org.checkerframework.checker.index.qual.NonNegative;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class CaffeineTest {

    /**
     * 手动加载cache
     */
    @DisplayName("手动加载cache的方式,设置maximumSize")
    @Test
    public void testManualLoadCache() throws InterruptedException {
        Cache<String, String> cache = Caffeine.newBuilder()
                .expireAfterWrite(5, TimeUnit.SECONDS) // 设置超时时间为5s / 写入后隔段时间过期
                .maximumSize(1)// 设置缓存最大条目数，超过条目则触发回收。
                .build();
        // 查找一个缓存元素， 没有查找到的时候返回null
        String value = cache.getIfPresent("test");
        System.out.println(value);//-->null
        // 查找缓存，如果缓存不存在则生成缓存元素,  如果无法生成则返回null
        value = cache.get("test", k -> "test-value");
        System.out.println(cache.getIfPresent("test"));//-->test-value
        System.out.println(value);//-->test-value


        //一次性往cache中添加大量的元素，使得超出缓存的最大数，触发缓存回收
        List<String> list = new ArrayList<>();
        for (int i = 2; i < 10000; i++) {
            list.add("test" + i);
        }
        for (int i = 2; i < 10000; i++) {
            // 添加或者更新一个缓存元素
            cache.put("test" + i, "test-value" + i);
        }

        Thread.sleep(2000);

        //根据key list去获取一个map的缓存
        Map<String, String> dataObjectMap
                = cache.getAllPresent(list);
        //查看缓存中的数据
        System.out.println(dataObjectMap.size()); //--> 1
        System.out.println(dataObjectMap); //--> {test9999=test-value9999}

        Thread.sleep(5000); //设置10s的睡眠时间，使得超过过期时间

        System.out.println(cache.getIfPresent("test"));//-->null
    }


    /**
     * 手动加载cache
     */
    @DisplayName("手动加载cache的方式,设置maximumSize,并使用clean up回收")
    @Test
    public void testManualLoadCache2() throws InterruptedException {
        Cache<String, String> cache = Caffeine.newBuilder()
                .expireAfterWrite(5, TimeUnit.SECONDS) // 设置超时时间为5s / 写入后隔段时间过期
                .maximumSize(1)// 设置缓存最大条目数，超过条目则触发回收。
                .build();
        // 查找一个缓存元素， 没有查找到的时候返回null
        String value = cache.getIfPresent("test");
        System.out.println(value);//-->null
        // 查找缓存，如果缓存不存在则生成缓存元素,  如果无法生成则返回null
        value = cache.get("test", k -> "test-value");
        System.out.println(cache.getIfPresent("test"));//-->test-value
        System.out.println(value);//-->test-value


        // 加入一些缓存数据
        List<String> list = new ArrayList<>();
        for (int i = 2; i < 10; i++) {
            list.add("test" + i);
        }
        for (int i = 2; i < 10; i++) {
            // 添加或者更新一个缓存元素
            cache.put("test" + i, "test-value" + i);
        }

        // 执行缓存回收
        //缓存的删除策略使用的是惰性删除和定时删除，但是我也可以自己调用cache.cleanUp()方法手动触发一次回收操作。cache.cleanUp()是一个同步方法。
        cache.cleanUp();


        //根据key list去获取一个map的缓存
        Map<String, String> dataObjectMap
                = cache.getAllPresent(list);
        //查看缓存中的数据
        System.out.println(dataObjectMap.size()); //--> 1
        System.out.println(dataObjectMap); //--> {test9=test-value9}

        Thread.sleep(5000); //设置10s的睡眠时间，使得超过过期时间

        System.out.println(cache.getIfPresent("test"));//-->null
    }


    @DisplayName("手动加载cache的方式,设置maximumWeight")
    @Test
    public void testManualLoadCache3() {
        Cache<String, List<Object>> stringListCache = Caffeine.newBuilder()
                //最大weight值，当所有entry的weight和快达到这个限制的时候会发生缓存过期，剔除一些缓存
                .maximumWeight(1)
                //每个 Entry 的 weight 值
                .weigher(new Weigher<String, List<Object>>() {
                    @Override
                    public @NonNegative int weigh(@NonNull String key, @NonNull List<Object> value) {
                        return value.size();
                    }
                })
                .build();

        List<Object> list = new ArrayList<>();
        stringListCache.put("test" + 1, Collections.singletonList("test1"));

        list.add("test" + 1);
        for (int i = 2; i < 10000; i++) {
            list.add("test" + i);
        }
        for (int i = 2; i < 10000; i++) {
            // 添加或者更新一个缓存元素
            stringListCache.put("test" + i, list);
        }

        Map<String, List<Object>> dataObjectMap = stringListCache.getAllPresent(list);
        System.out.println(dataObjectMap.size()); // --> 1
        System.out.println(dataObjectMap); // --> {test1=[test1]}
    }


    @DisplayName("手动加载cache的方式,设置maximumWeight,并使用clean up回收")
    @Test
    public void testManualLoadCache4() {
        Cache<String, List<Object>> stringListCache = Caffeine.newBuilder()
                //最大weight值，当所有entry的weight和快达到这个限制的时候会发生缓存过期，剔除一些缓存
                .maximumWeight(1)
                //每个 Entry 的 weight 值
                .weigher(new Weigher<String, List<Object>>() {
                    @Override
                    public @NonNegative int weigh(@NonNull String key, @NonNull List<Object> value) {
                        return value.size();
                    }
                })
                .build();

        stringListCache.put("test1", Collections.singletonList("test-value1"));
        stringListCache.put("test2", Arrays.asList("test-value2", "test-value2"));


        stringListCache.cleanUp();

        Map<String, List<Object>> dataObjectMap = stringListCache.getAllPresent(Arrays.asList("test1", "test2"));
        System.out.println(dataObjectMap.size()); // --> 1
        System.out.println(dataObjectMap); // --> {test1=[test-value1]}
    }

    @DisplayName("手动加载cache的方式,设置maximumWeight,maximumWeight和maximumSize等效的情况")
    @Test
    public void testManualLoadCache5() {
        Cache<String, List<Object>> stringListCache = Caffeine.newBuilder()
                //最大weight值，当所有entry的weight和快达到这个限制的时候会发生缓存过期，剔除一些缓存
                .maximumWeight(2)
                //每个 Entry 的 weight 值
                .weigher(new Weigher<String, List<Object>>() {
                    @Override
                    public @NonNegative int weigh(@NonNull String key, @NonNull List<Object> value) {
                        return 1; //当把 Weigher 实现为只返回1，maximumWeight 其实和 maximumSize 是等效的。
                    }
                })
                .build();

        stringListCache.put("test1", Collections.singletonList("test-value1"));
        stringListCache.put("test2", Arrays.asList("test-value2", "test-value2"));


        stringListCache.cleanUp();

        Map<String, List<Object>> dataObjectMap = stringListCache.getAllPresent(Arrays.asList("test1", "test2"));
        System.out.println(dataObjectMap.size()); // --> 1
        System.out.println(dataObjectMap); // --> {test1=[test-value1]}
    }


    @DisplayName("基于大小的过期策略")
    @Test
    public void testManualLoadCache6() {
        Cache<String, String> cache = Caffeine.newBuilder()
                .maximumSize(1)
                .build();
        cache.put("key1", "value1");
        cache.put("key2", "value2");

        System.out.println(cache.getIfPresent("key1"));
        System.out.println(cache.getIfPresent("key2"));

        cache.cleanUp();

        System.out.println(cache.getIfPresent("key1"));
        System.out.println(cache.getIfPresent("key2"));
    }


    @DisplayName("基于时间的过期策略,设置expireAfterWrite")
    @Test
    public void testManualLoadCache7() throws InterruptedException {
        //在最后一次写入缓存后开始计时，在指定的时间后过期。
        Cache<String, String> cache = Caffeine.newBuilder()
                .expireAfterWrite(3, TimeUnit.SECONDS)
                .build();
        cache.put("key1", "value1");
        Thread.sleep(1000);
        System.out.println(cache.getIfPresent("key1")); // -> value1
        Thread.sleep(1000);
        System.out.println(cache.getIfPresent("key1")); // -> value1
        Thread.sleep(1000);
        System.out.println(cache.getIfPresent("key1")); // -> null
    }

    @DisplayName("基于时间的过期策略,设置expireAfterAccess")
    @Test
    public void testManualLoadCache8() throws InterruptedException {
        // 在最后一次读或者写入后开始计时，在指定的时间后过期。假如一直有请求访问该key，那么这个缓存将一直不会过期。
        Cache<String, String> cache = Caffeine.newBuilder()
                .expireAfterAccess(3, TimeUnit.SECONDS)
                .build();
        cache.put("key1", "value1");
        Thread.sleep(1000);
        System.out.println(cache.getIfPresent("key1")); // -> value1
        Thread.sleep(1000);
        System.out.println(cache.getIfPresent("key1")); // -> value1
        Thread.sleep(1000);
        System.out.println(cache.getIfPresent("key1")); // -> value1
        Thread.sleep(3001);
        System.out.println(cache.getIfPresent("key1")); // -> null
    }


    @DisplayName("基于时间的过期策略,设置expireAfterCreate")
    @Test
    public void testManualLoadCache9() throws InterruptedException {
        Cache<String, String> cache = Caffeine.newBuilder()
                .expireAfter(new Expiry<String, String>() {
                    @Override
                    public long expireAfterCreate(@NonNull String key, @NonNull String value, long currentTime) {
                        return TimeUnit.SECONDS.toNanos(3);
                    }

                    @Override
                    public long expireAfterUpdate(@NonNull String key, @NonNull String value, long currentTime, @NonNegative long currentDuration) {
                        return currentDuration;
                    }

                    @Override
                    public long expireAfterRead(@NonNull String key, @NonNull String value, long currentTime, @NonNegative long currentDuration) {
                        return currentDuration;
                    }
                })
                .build();
        cache.put("key1", "value1");
        Thread.sleep(1000);
        System.out.println(cache.getIfPresent("key1")); // -> value1
        Thread.sleep(1000);
        System.out.println(cache.getIfPresent("key1")); // -> value1
        Thread.sleep(1000);
        System.out.println(cache.getIfPresent("key1"));  // -> null
    }


    @DisplayName("基于时间的过期策略,设置expireAfterUpdate")
    @Test
    public void testManualLoadCache10() throws InterruptedException {
        Cache<String, String> cache = Caffeine.newBuilder()
                .expireAfter(new Expiry<String, String>() {
                    @Override
                    public long expireAfterCreate(@NonNull String key, @NonNull String value, long currentTime) {
                        return Long.MAX_VALUE;
                    }

                    @Override
                    public long expireAfterUpdate(@NonNull String key, @NonNull String value, long currentTime, @NonNegative long currentDuration) {
                        return TimeUnit.SECONDS.toNanos(3);
                    }

                    @Override
                    public long expireAfterRead(@NonNull String key, @NonNull String value, long currentTime, @NonNegative long currentDuration) {
                        return currentDuration;
                    }
                })
                .build();
        cache.put("key1", "value1");
        cache.put("key1", "value2");
        Thread.sleep(1000);
        System.out.println(cache.getIfPresent("key1")); // -> value2
        Thread.sleep(1000);
        System.out.println(cache.getIfPresent("key1")); // -> value2
        Thread.sleep(1000);
        System.out.println(cache.getIfPresent("key1"));  // -> null
    }


    @DisplayName("基于时间的过期策略,设置expireAfterRead")
    @Test
    public void testManualLoadCache11() throws InterruptedException {
        Cache<String, String> cache = Caffeine.newBuilder()
                .expireAfter(new Expiry<String, String>() {
                    @Override
                    public long expireAfterCreate(@NonNull String key, @NonNull String value, long currentTime) {
                        return Long.MAX_VALUE;
                    }

                    @Override
                    public long expireAfterUpdate(@NonNull String key, @NonNull String value, long currentTime, @NonNegative long currentDuration) {
                        return currentDuration;
                    }

                    @Override
                    public long expireAfterRead(@NonNull String key, @NonNull String value, long currentTime, @NonNegative long currentDuration) {
                        return TimeUnit.SECONDS.toNanos(3);
                    }
                })
                .build();
        cache.put("key1", "value1");
        Thread.sleep(1000);
        System.out.println(cache.getIfPresent("key1")); // -> value1
        Thread.sleep(1000);
        System.out.println(cache.getIfPresent("key1")); // -> value1
        Thread.sleep(1000);
        System.out.println(cache.getIfPresent("key1")); // -> value1
        Thread.sleep(3001);
        System.out.println(cache.getIfPresent("key1")); // -> null
    }

}
